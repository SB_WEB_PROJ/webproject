package com.brk.springboot.web;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

@Component
public class ServiceInterceptor implements HandlerInterceptor {
	   @Override
	   public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) 
	      throws Exception {
	      System.out.println("Pre Handle method is Called "+new Date());
	      return true;
	   }
	   @Override
	   public void postHandle(HttpServletRequest request, HttpServletResponse response, 
	      Object handler, ModelAndView modelAndView) throws Exception {
	      System.out.println("Post Handle method is Called "+new Date());
	   }
	   @Override
	   public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object 
	      handler, Exception exception) throws Exception {
	      
	      System.out.println("Request and Response is completed "+new Date());
	   }
}
